package com.example.v7t5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    EditText filename;
    EditText text;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void save(View s) {
        text = (EditText) findViewById(R.id.editText1);
        filename = (EditText) findViewById(R.id.editText2);
        file = new File(filename.getText().toString());
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(text.getText().toString());
        }catch (FileNotFoundException ex) {
            System.out.println("Tiedostoa ei löydy");
        } catch (IOException ex) {
            System.out.println("Virhe");
        }
    }

    public void load(View l) {
        filename = (EditText) findViewById(R.id.editText2);
        file = new File(filename.getText().toString());
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.setText(line);
            }
        }catch (FileNotFoundException ex) {
            System.out.println("Tiedostoa ei löydy");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
